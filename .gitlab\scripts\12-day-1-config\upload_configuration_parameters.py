"""
Setup the first day configuration for the NRF
"""

from ana_toolbox.commands import RunCommand


def get_yang_service_name() -> str:
    """Get the service name from the ericson culster. The cooman from the documentation:
    kubectl get svc -n $namespace |grep eric-cm-yang-provider|grep LoadBalancer|awk '{ print $1 }'| head -n1
    """


def get_yang_port_netconf(yang_service_name: str) -> str:
    """Get the netconf port from the service in the ericson culster. The cooman from the documentation:
    kubectl get svc $YANG_SERVICE_NAME -o jsonpath='{.spec.ports[?(@.name=="netconf")].port }' -n $namespace
    """


def setup_yang() -> None:
    """Run the configuration command via ssh. The cooman from the documentation:
    ssh -t -p $YANG_PORT_NETCONF <CM Yang Provider USER>@<CCRC_OAM_VIP> -s netconf < get_product-nf_cm.xml
    <CCRC_OAM_VIP> is defined in the infrastructure, as indicated during deployment.
    """


if __name__ == "__main__":
    service_name = get_yang_service_name()
    yang_port_netconf = get_yang_port_netconf(service_name)
